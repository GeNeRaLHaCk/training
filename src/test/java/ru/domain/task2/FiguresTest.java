package ru.domain.task2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class FiguresTest {
    @Test
    public void calculateArea(){
        List<IFigure> figures = new ArrayList<>();
        figures.add(new Rectangle(5, 10));
        figures.add(new Circle(5));
        figures.add(new Square(5));
        double result = figures.stream().
                map(figure-> figure.calculateArea()).
                mapToDouble(f -> f).sum();
        double expected = 153.5;
        assertEquals(expected, result, 0);
    }
}
