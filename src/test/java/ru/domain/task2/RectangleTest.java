package ru.domain.task2;

import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    @Test
    public void calculateArea() {
        Rectangle rectangle = new Rectangle(5, 10);
        double result = rectangle.calculateArea();
        double expected = 50;
        assertEquals(expected, result, 0);
    }
}