package ru.domain.task2;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTest {

    @Test
    public void calculateArea() {
        Square square = new Square(5);
        double result = square.calculateArea();
        double expected = 25;
        assertEquals(expected, result, 0);
    }
}