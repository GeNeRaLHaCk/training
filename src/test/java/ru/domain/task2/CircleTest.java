package ru.domain.task2;

import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {

    @Test
    public void calculateArea() {
        Circle circle = new Circle(5);
        double result = circle.calculateArea();
        double expected = 78.5;
        assertEquals(expected, result, 0);
    }
}