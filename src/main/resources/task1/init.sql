create table book
(
    id   serial      not null
        constraint book_pkey
            primary key,
    name varchar(50) not null
);

create table author
(
    id         serial      not null
        constraint author_pkey
            primary key,
    firstname  varchar(50) not null,
    lastname   varchar(50) not null,
    middlename varchar(50) not null
);

create table book_author
(
    bookid   integer not null
        constraint book_author_book_id_fk
            references book
            on delete cascade,
    authorid integer not null
        constraint book_author_author_id_fk
            references author
            on delete cascade
);

INSERT INTO public.author (id, firstname, lastname, middlename) VALUES (1, 'Иван', 'Иванов', 'Иванович');
INSERT INTO public.author (id, firstname, lastname, middlename) VALUES (2, 'Петр', 'Шляпников', 'Петрович');
INSERT INTO public.book (id, name) VALUES (1, 'Родные просторы');
INSERT INTO public.book (id, name) VALUES (2, 'Летние поля');
INSERT INTO public.book (id, name) VALUES (3, 'Шагай в даль');
INSERT INTO public.book (id, name) VALUES (4, 'Новая республика');
INSERT INTO public.book_author (bookid, authorid) VALUES (1, 1);
INSERT INTO public.book_author (bookid, authorid) VALUES (2, 1);
INSERT INTO public.book_author (bookid, authorid) VALUES (3, 2);
INSERT INTO public.book_author (bookid, authorid) VALUES (4, 2);