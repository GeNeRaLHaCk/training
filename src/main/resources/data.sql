insert into person(id, sex, name, age, weight, height)
values (10001,'male', 'John', 18, 110, 200);
insert into person(id, sex, name, age, weight, height)
values (10002,'male', 'Kirill', 20, 100, 2);
insert into person(id, sex, name, age, weight, height)
values (10003,'male', 'Valentin', 25, 75, 175);
insert into person(id, sex, name, age, weight, height)
values (10004,'male', 'Arkasha', 23, 85, 190);
insert into person(id, sex, name, age, weight, height)
values (10005,'female', 'Dasha', 33, 63, 165);