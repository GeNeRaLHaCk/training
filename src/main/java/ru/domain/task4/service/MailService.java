package ru.domain.task4.service;

/**
 * Created by bshestakov on 07.11.2017.
 */
public class MailService {

    public void send(String title, String body) {
        System.out.println("message is sent by mail. title: " +
                title + " body: " + body);
    }
}
