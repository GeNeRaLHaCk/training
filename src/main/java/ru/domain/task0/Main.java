package ru.domain.task0;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bshestakov on 07.11.2017.
 */
public class Main {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Dmitry", 20L));
        persons.add(new Person("Valery", 30L));
        persons.add(new Person("Arkasha", 18L));
        persons.add(new Person("Mumrik", 17L));
        System.out.println(adultPersons(persons));
    }
    /**
     * Возвращает всех людей старше 18 лет.
     *
     * @param persons список людей
     * @return список людей старше 18 лет
     */
    public static List<Person> adultPersons(List<Person> persons) {
        List<Person> listPerson = new ArrayList<>();
        listPerson = persons.stream().filter(x ->
                x.age() >= 18).collect(Collectors.toList());
        return listPerson;
    }
}
