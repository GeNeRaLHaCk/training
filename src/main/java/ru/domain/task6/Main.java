package ru.domain.task6;

import org.apache.commons.io.FileUtils;
import ru.domain.task6.report.docx.DocGeneratorUtils;
import ru.domain.task6.report.service.JobOfferReportService;
import ru.domain.task6.report.domain.JobOfferParams;
import ru.domain.task6.report.domain.JobOfferReportFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.wml.Text;
/**
 * Изменить приложение так, чтобы документ формировался с блоком
 * зарплата описанным в документе Job_offer_liga_new_req.docx.
 * Значение зарплаты должно быть параметризуемым.
 */
public class Main {
    public static void main(String[] args) throws Docx4JException, IOException {
        String salary = "";
        List<Object> texts = DocGeneratorUtils.getAllElementFromObject(
        DocGeneratorUtils.getTemplate(
                "Job_offer_liga_new_req.docx")
                .getMainDocumentPart().getJaxbElement(),Text.class);
        for (int i = 0; i < texts.size(); i++) {
            Text textElement = (Text) texts.get(i);
            if (textElement.getValue().contains("salary")){
                Text salaryText = (Text)texts.get(i + 2);
                salary = salaryText.getValue();
            }
        }
        JobOfferReportFile jobOfferReportFile =
                new JobOfferReportService().buildReport(
                new JobOfferParams("Дмитрий",
                        "Разработчик",
                        "БКС",
                        "ITSM",
                        "три месяца",
                        "Сбербанк", salary));
        FileUtils.writeByteArrayToFile(
                new File(jobOfferReportFile.getFilename()),
                jobOfferReportFile.getContent());
    }
}