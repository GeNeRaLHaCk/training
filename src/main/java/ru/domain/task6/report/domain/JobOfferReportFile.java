package ru.domain.task6.report.domain;

public class JobOfferReportFile {
    private String filename;
    private byte[] content;


    public JobOfferReportFile(String filename, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getContent() {
        return content;
    }
}
