package ru.domain.task6.report.docx;

import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

/**
 * Кэш с темплейтами для отчётов
 */
public class WordTemplatesCache {
    private static WordprocessingMLPackage template;

    /**
     * Возвращает из кэша копию шаблона для отчёта на русском языке
     * Если в кэше такого шаблона нет, то пытается создать такой шаблон
     */
    public static WordprocessingMLPackage getOrCreateTemplate(String templateName) {
        if (template == null) {
            template = DocGeneratorUtils.
                    getTemplate(
                            templateName);
        }
        return (WordprocessingMLPackage) template.clone();
    }


}
