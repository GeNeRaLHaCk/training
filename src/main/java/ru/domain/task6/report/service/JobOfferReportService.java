package ru.domain.task6.report.service;


import org.docx4j.openpackaging.exceptions.Docx4JException;
import ru.domain.task6.report.docx.DocGeneratorUtils;
import ru.domain.task6.report.domain.JobOfferParams;
import ru.domain.task6.report.domain.JobOfferReport;
import ru.domain.task6.report.domain.JobOfferReportFile;

public class JobOfferReportService {

    public JobOfferReportFile buildReport(JobOfferParams params) throws Docx4JException {
        return new JobOfferReport(params).docx("Job_offer_liga_template.docx");
    }
}
