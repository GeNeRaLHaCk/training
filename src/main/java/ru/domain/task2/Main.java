package ru.domain.task2;

import java.util.ArrayList;
import java.util.List;

/**
 * Задача. На входе есть N геометрических фигур (например, круг, квадрат). Приложение должно считать
 * суммарную площадь всех фигур и печатать её на экране. Спроектировать в парадигме ООП.
 * Написать тесты.
 * <p>
 * Площадь круга - 3.14*R^2
 * Площадь квадрата - A^2
 * Площадь прямоугольника -A*B
 */
public class Main {
    public static void main(String[] args) {
        List<IFigure> figures = new ArrayList<>();
        figures.add(new Rectangle(5, 10));
        figures.add(new Circle(5));
        figures.add(new Square(5));
        double sumAreas = figures.stream().
                map(x->x.calculateArea()).mapToDouble(f -> f).sum();
        System.out.println(sumAreas);
    }
}
