package ru.domain.task2;

public class Circle implements IFigure{
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calculateArea() {
        return 3.14 * (r*r);
    }
}
