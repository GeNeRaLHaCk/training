package ru.domain.task2;

public interface IFigure {
    double calculateArea();
}
