package ru.domain.task3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.domain.task3.domain.Person;
import ru.domain.task3.repository.PersonRepository;

import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    /**
     * Возвращает средний индекс массы тела всех лиц мужского пола старше 18 лет
     *
     * @return
     */
    public String getAdultMaleUsersAverageBMI() {
        double totalImt = 0.0;
        long countOfPerson = 0;
        double heightInMeters = 0d;
        double imt = 0d;
        List<Person> persons = personRepository.findAllBySexAndAge("male", 18L);
        for (Person p : persons) {
            heightInMeters = p.getHeight() / 100d;
            imt = p.getWeight() / (Double) (heightInMeters * heightInMeters);
            totalImt += imt;
        }
        countOfPerson = persons.size();
        return "Average imt - " + totalImt / countOfPerson;
    }
}
