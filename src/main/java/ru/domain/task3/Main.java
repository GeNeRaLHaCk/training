package ru.domain.task3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource({"classpath:task3/application.properties"})
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }
}
