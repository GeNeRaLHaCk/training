package ru.domain.task3.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.domain.task3.domain.Person;
import ru.domain.task3.service.PersonService;

@RestController
public class PersonController {
    @Autowired
    public PersonService personService;
    @GetMapping("/male/averageIndexMass")
    public String dd(){
        return personService.getAdultMaleUsersAverageBMI();
    }
}
